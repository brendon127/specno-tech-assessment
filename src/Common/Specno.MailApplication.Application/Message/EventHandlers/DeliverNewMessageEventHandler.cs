using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Specno.Domain.Events;
using Specno.MailApplication.Application.Common.Interfaces;
using Specno.MailApplication.Application.Common.Models;

namespace Specno.MailApplication.Application.Message.EventHandlers
{
    public class DeliverNewMessageEventHandler : INotificationHandler<DomainEventNotification<NewMessageCreatedEvent>>
    {
        private readonly IMailDeliveryService _mailDeliveryService;

        public DeliverNewMessageEventHandler(IMailDeliveryService mailDeliveryService)
        {
            _mailDeliveryService = mailDeliveryService;
        }

        public Task Handle(DomainEventNotification<NewMessageCreatedEvent> notification,
            CancellationToken cancellationToken)
        {
            var message = notification.DomainEvent.Message;
            _mailDeliveryService.Deliver(message);
            return Task.CompletedTask;
        }
    }
}