using System;
using System.Collections.Generic;
using Specno.Domain.ValueObjects;
using Specno.MailApplication.Application.Common.Interfaces;

namespace Specno.MailApplication.Application.Message.Queries
{
    public class MessageDto
    {
        public Guid Id { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public IEnumerable<EmailAddress> ToList { get; set; }
        public IEnumerable<EmailAddress> CcList { get; set; }
        public IEnumerable<EmailAddress> BccList { get; set; }
        public Guid ConversationId { get; set; }
    }

    public class GetMessagesQuery : IRequestWrapper<IEnumerable<MessageDto>>
    {
        public string LabelId { get; set; }
    }
}