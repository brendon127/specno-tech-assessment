using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Specno.Domain.Entities.InboxOwner;

namespace Specno.MailApplication.Application.Message.Commands.RecoverFromTrash
{
    public class RecoverMessageFromTrashCommand : IRequest
    {
        public Guid MessageId { get; set; }
    }

    public class RecoverMessageFromTrashCommandHandler : IRequestHandler<RecoverMessageFromTrashCommand>
    {
        private readonly IInboxOwnerRepository _inboxOwnerRepository;

        public RecoverMessageFromTrashCommandHandler(IInboxOwnerRepository inboxOwnerRepository)
        {
            _inboxOwnerRepository = inboxOwnerRepository;
        }

        public async Task<Unit> Handle(RecoverMessageFromTrashCommand request, CancellationToken cancellationToken)
        {
            var inboxOwner = _inboxOwnerRepository.GetCurrentInboxOwner();
            inboxOwner.RecoverMessage(request.MessageId);
            await _inboxOwnerRepository.UpdateAsync(inboxOwner);
            return Unit.Value;
        }
    }
}