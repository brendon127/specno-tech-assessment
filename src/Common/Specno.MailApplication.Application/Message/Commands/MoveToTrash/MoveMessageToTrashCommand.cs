using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Specno.Domain.Entities.InboxOwner;

namespace Specno.MailApplication.Application.Message.Commands.MoveToTrash
{
    public class MoveMessageToTrashCommand : IRequest
    {
        public Guid MessageId { get; set; }
    }

    public class MoveToTrashCommandHandler : IRequestHandler<MoveMessageToTrashCommand>
    {
        private readonly IInboxOwnerRepository _inboxOwnerRepository;

        public MoveToTrashCommandHandler(IInboxOwnerRepository inboxOwnerRepository)
        {
            _inboxOwnerRepository = inboxOwnerRepository;
        }

        public async Task<Unit> Handle(MoveMessageToTrashCommand request, CancellationToken cancellationToken)
        {
            var inboxOwner = _inboxOwnerRepository.GetCurrentInboxOwner();
            inboxOwner.MoveMessageToTrash(request.MessageId);
            await _inboxOwnerRepository.UpdateAsync(inboxOwner);
            return Unit.Value;
        }
    }
}