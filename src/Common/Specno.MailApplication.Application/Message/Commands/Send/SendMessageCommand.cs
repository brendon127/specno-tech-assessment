using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Specno.Domain.Entities.InboxOwner;
using Specno.Domain.Entities.Message;
using Specno.Domain.ValueObjects;

namespace Specno.MailApplication.Application.Message.Commands.Send
{
    public class SendMessageCommand : IRequest
    {
        public string Body { get; set; }
        public string Subject { get; set; }
        public string FromEmail { get; set; }
        public IEnumerable<string> ToList { get; set; }
        public IEnumerable<string> CcList { get; set; }
        public IEnumerable<string> BccList { get; set; }
    }

    public class SendMessageCommandHandler : IRequestHandler<SendMessageCommand>
    {
        private readonly MessageBuilder _messageBuilder;
        private readonly IInboxOwnerRepository _inboxOwnerRepository;
        private SendMessageCommand _request;

        public SendMessageCommandHandler(MessageBuilder messageBuilder, IInboxOwnerRepository inboxOwnerRepository)
        {
            _messageBuilder = messageBuilder;
            _inboxOwnerRepository = inboxOwnerRepository;
        }

        public async Task<Unit> Handle(SendMessageCommand request, CancellationToken cancellationToken)
        {
            _request = request;

            var message = await CreateNewMessage();
            var inboxOwner = _inboxOwnerRepository.GetCurrentInboxOwner();
            inboxOwner.SendMessage(message);
            await _inboxOwnerRepository.UpdateAsync(inboxOwner);
            return Unit.Value;
        }

        private async Task<Specno.Domain.Entities.Message.Message> CreateNewMessage()
        {
            var message = await _messageBuilder
                .WithSender(new EmailAddress(_request.FromEmail, _request.FromEmail))
                .WithSubject(_request.Subject)
                .WithBody(_request.Body)
                .WithRecipients(new MessageRecipients
                {
                    ToList = _request.ToList.Select(email => new EmailAddress(email, email)).ToList(),
                    CcList = _request.CcList.Select(email => new EmailAddress(email, email)).ToList(),
                    BccList = _request.BccList.Select(email => new EmailAddress(email, email)).ToList(),
                })
                .WithReplyRecipients(new ReplyRecipients
                {
                    ReplyTo = new EmailAddress(_request.FromEmail, _request.FromEmail),
                    ReplyTos = _request.ToList.Select(email => new EmailAddress(email, email)).ToList(),
                })
                .BuildAsync();

            return message;
        }
    }
}