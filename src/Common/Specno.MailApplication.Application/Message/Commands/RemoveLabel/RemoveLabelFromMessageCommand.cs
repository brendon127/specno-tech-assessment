using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Newtonsoft.Json;
using Specno.Domain.Entities.InboxOwner;

namespace Specno.MailApplication.Application.Message.Commands.RemoveLabel
{
    public class RemoveLabelFromMessageCommand : IRequest
    {
        public Guid LabelId { get; set; }
        [JsonIgnore]
        public Guid MessageId { get; set; }
        
    }
    
    public class RemoveLabelFromMessageCommandHandler : IRequestHandler<RemoveLabelFromMessageCommand>
    {
        private readonly IInboxOwnerRepository _inboxOwnerRepository;

        public RemoveLabelFromMessageCommandHandler(IInboxOwnerRepository inboxOwnerRepository)
        {
            _inboxOwnerRepository = inboxOwnerRepository;
        }

        public Task<Unit> Handle(RemoveLabelFromMessageCommand request, CancellationToken cancellationToken)
        {
            var inboxOwner = _inboxOwnerRepository.GetCurrentInboxOwner();
            inboxOwner.RemoveLabelFromMessage(request.LabelId, request.MessageId);
            _inboxOwnerRepository.UpdateAsync(inboxOwner);
            return Unit.Task;
        }
    }
}