using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Newtonsoft.Json;
using Specno.Domain.Entities.InboxOwner;

namespace Specno.MailApplication.Application.Message.Commands.AddLabel
{
    public class AddLabelToMessageCommand : IRequest
    {
        public Guid LabelId { get; set; }
        [JsonIgnore]
        public Guid MessageId { get; set; }
    }
    
    public class AddLabelToMessageCommandHandler : IRequestHandler<AddLabelToMessageCommand>
    {
        private readonly IInboxOwnerRepository _inboxOwnerRepository;

        public AddLabelToMessageCommandHandler(IInboxOwnerRepository inboxOwnerRepository)
        {
            _inboxOwnerRepository = inboxOwnerRepository;
        }

        public async Task<Unit> Handle(AddLabelToMessageCommand request, CancellationToken cancellationToken)
        {
            var inboxOwner = _inboxOwnerRepository.GetCurrentInboxOwner();
            inboxOwner.AddLabelToMessage(request.LabelId, request.MessageId);
            await _inboxOwnerRepository.UpdateAsync(inboxOwner);
            return Unit.Value;
        }
    }
}