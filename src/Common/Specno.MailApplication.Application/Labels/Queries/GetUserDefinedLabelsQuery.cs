using System;
using System.Collections.Generic;
using Specno.MailApplication.Application.Common.Interfaces;

namespace Specno.MailApplication.Application.Labels.Queries
{
    public class UserDefinedLabelDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }

    public class GetUserDefinedLabelsQuery : IRequestWrapper<IEnumerable<UserDefinedLabelDto>>
    {
    }
}