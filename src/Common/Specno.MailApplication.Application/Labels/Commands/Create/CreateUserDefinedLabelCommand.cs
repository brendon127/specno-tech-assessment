using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Specno.Domain.Entities.InboxOwner;
using Specno.Domain.Entities.Label;

namespace Specno.MailApplication.Application.Labels.Commands.Create
{
    public class CreateUserDefinedLabelCommand : IRequest
    {
        public string Name { get; set; }
    }

    public class CreateUserDefinedLabelCommandHandler : IRequestHandler<CreateUserDefinedLabelCommand>
    {
        private readonly IInboxOwnerRepository _inboxOwnerRepository;

        public CreateUserDefinedLabelCommandHandler(IInboxOwnerRepository inboxOwnerRepository)
        {
            _inboxOwnerRepository = inboxOwnerRepository;
        }

        public async Task<Unit> Handle(CreateUserDefinedLabelCommand request, CancellationToken cancellationToken)
        {
            var inboxOwner = _inboxOwnerRepository.GetCurrentInboxOwner();
            var newLabel = new UserDefinedLabel(request.Name);
            inboxOwner.CreateLabel(newLabel);
            await _inboxOwnerRepository.UpdateAsync(inboxOwner);
            return Unit.Value;
        }
    }
}