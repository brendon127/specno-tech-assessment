using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Specno.Domain.Entities.InboxOwner;

namespace Specno.MailApplication.Application.Labels.Commands.Delete
{
    public class DeleteUserDefinedLabelCommand : IRequest
    {
        public Guid LabelId { get; set; }
    }

    public class DeleteUserDefinedLabelCommandHandler : IRequestHandler<DeleteUserDefinedLabelCommand>
    {
        private readonly IInboxOwnerRepository _inboxOwnerRepository;

        public DeleteUserDefinedLabelCommandHandler(IInboxOwnerRepository inboxOwnerRepository)
        {
            _inboxOwnerRepository = inboxOwnerRepository;
        }

        public Task<Unit> Handle(DeleteUserDefinedLabelCommand request, CancellationToken cancellationToken)
        {
            var inboxOwner = _inboxOwnerRepository.GetCurrentInboxOwner();
            inboxOwner.DeleteLabel(request.LabelId);
            _inboxOwnerRepository.UpdateAsync(inboxOwner);
            return Unit.Task;
        }
    }
}