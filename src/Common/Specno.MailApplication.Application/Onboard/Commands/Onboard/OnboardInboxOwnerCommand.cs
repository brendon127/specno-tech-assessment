using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Specno.Domain.Entities.InboxOwner;
using Specno.MailApplication.Application.Common.Interfaces;

namespace Specno.MailApplication.Application.Onboard.Commands.Onboard
{
    public class OnboardInboxOwnerCommand : IRequest
    {
    }

    public class OnboardInboxOwnerCommandHandler : IRequestHandler<OnboardInboxOwnerCommand>
    {
        private readonly ICurrentUserService _currentUserService;
        private readonly IInboxOwnerRepository _inboxOwnerRepository;

        public OnboardInboxOwnerCommandHandler(ICurrentUserService currentUserService,
            IInboxOwnerRepository inboxOwnerRepository)
        {
            _currentUserService = currentUserService;
            _inboxOwnerRepository = inboxOwnerRepository;
        }

        public async Task<Unit> Handle(OnboardInboxOwnerCommand request, CancellationToken cancellationToken)
        {
            var exists = _inboxOwnerRepository
                .GetAll()
                .FirstOrDefault(s => s.UserId == _currentUserService.UserId);
            if (exists != null)
            {
                return Unit.Value;
            }

            var newOwner = new InboxOwner(_currentUserService.UserId);
            newOwner.AddEmailAddress(_currentUserService.Email);
            await _inboxOwnerRepository.AddAsync(newOwner);
            return Unit.Value;
        }
    }
}