﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Specno.MailApplication.Application.Common.Models;

namespace Specno.MailApplication.Application.Common.Mapping
{
    public static class MappingExtensions
    {
        public static PaginatedList<TDestination> PaginatedListAsync<TDestination>(
            this IQueryable<TDestination> queryable, int pageNumber, int pageSize)
            => PaginatedList<TDestination>.Create(queryable, pageNumber, pageSize);

        public static List<TDestination> ProjectToList<TDestination>(this IQueryable queryable,
            IConfigurationProvider configuration, CancellationToken cancellationToken)
            => queryable.ProjectTo<TDestination>(configuration).ToList();
    }
}