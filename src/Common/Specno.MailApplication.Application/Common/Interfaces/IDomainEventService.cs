﻿using System.Threading.Tasks;
using Specno.Domain.Common;

namespace Specno.MailApplication.Application.Common.Interfaces
{
    public interface IDomainEventService
    {
        Task Publish(DomainEvent domainEvent);
    }
}
