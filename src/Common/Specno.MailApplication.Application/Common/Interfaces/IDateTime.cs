﻿using System;

namespace Specno.MailApplication.Application.Common.Interfaces
{
    public interface IDateTime
    {
        DateTime Now { get; }
    }
}
