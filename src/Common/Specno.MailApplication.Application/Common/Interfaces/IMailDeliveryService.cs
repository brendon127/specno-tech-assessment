namespace Specno.MailApplication.Application.Common.Interfaces
{
    public interface IMailDeliveryService
    {
        public void Deliver(Specno.Domain.Entities.Message.Message message);
    }
}