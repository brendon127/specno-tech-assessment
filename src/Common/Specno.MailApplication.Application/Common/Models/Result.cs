﻿using System.Collections.Generic;

namespace Specno.MailApplication.Application.Common.Models
{
    public class Result
    {
        internal Result(bool succeeded, IDictionary<string, string[]> errors)
        {
            Succeeded = succeeded;
            Errors = errors;
        }

        public bool Succeeded { get; set; }

        public IDictionary<string, string[]> Errors { get; set; }

        public static Result Success()
        {
            return new Result(true, new Dictionary<string, string[]>());
        }

        public static Result Failure(Dictionary<string, string[]> errors)
        {
            return new Result(false, errors);
        }
    }
}