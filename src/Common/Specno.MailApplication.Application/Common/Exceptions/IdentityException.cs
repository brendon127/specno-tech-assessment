﻿using System;
using System.Collections.Generic;

namespace Specno.MailApplication.Application.Common.Exceptions
{
    public class IdentityException : Exception
    {
        public IDictionary<string, string[]> Errors { get; set; }

        public IdentityException(IDictionary<string, string[]> errors) : base()
        {
            Errors = errors;
        }
    }
}