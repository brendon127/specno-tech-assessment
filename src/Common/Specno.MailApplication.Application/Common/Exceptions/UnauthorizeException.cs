﻿using System;

namespace Specno.MailApplication.Application.Common.Exceptions
{
    public class UnauthorizeException : Exception
    {
        public UnauthorizeException(string message="") : base(message)
        {
            
        }
    }
}
