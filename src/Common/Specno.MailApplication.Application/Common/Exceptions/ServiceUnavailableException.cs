﻿using System;

namespace Specno.MailApplication.Application.Common.Exceptions
{
    public class ServiceUnavailableException : Exception
    {
        public ServiceUnavailableException() : base() { }
    }
}
