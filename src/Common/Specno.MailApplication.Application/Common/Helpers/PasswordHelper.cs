using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Specno.MailApplication.Application.Common.Helpers
{
    public static class PasswordHelper
    {
        private static readonly char[] CharsLowercaseLetters =
            @"abcdefghijklmnopqrstuvwxyz".ToCharArray();

        private static readonly char[] CharsUppercaseLetters =
            @"ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();

        private static readonly char[] CharsNumbers =
            @"0123456789".ToCharArray();

        private static readonly char[] CharsSpecials =
            @"!#$%&'()*+,-./:;<=>?@[\]^_`{|}~".ToCharArray();

        public static string GeneratePassword(int passwordLength,
            string validCharacters)
        {
            if (string.IsNullOrEmpty(validCharacters))
                throw new SystemException("Not a valid request! There has to be some characters allowed.");
            var randomBytes = new byte[passwordLength];
            using (var provider = new RNGCryptoServiceProvider()) provider.GetBytes(randomBytes);
            var result = new StringBuilder(passwordLength);
            foreach (byte b in randomBytes) result.Append(validCharacters[b % validCharacters.Length]);
            return result.ToString();
        }

        public static string GeneratePassword(int passwordLength,
            bool allowLowecaseLetters = true, bool allowUppercaseLetters = true,
            bool allowNumbers = true, bool allowSpecials = true)
        {
            var characters = new List<char>();
            if (allowNumbers) characters.AddRange(CharsNumbers);
            if (allowLowecaseLetters) characters.AddRange(CharsLowercaseLetters);
            if (allowUppercaseLetters) characters.AddRange(CharsUppercaseLetters);
            if (allowSpecials) characters.AddRange(CharsSpecials);
            return GeneratePassword(passwordLength, new string(characters.ToArray()));
        }

        public static string GeneratePassword(int passwordLength = 8,
            int minimumLowercaseLetters = 1, int minimumUppercaseLetters = 1,
            int minimumNumbers = 1, int minimumSpecials = 1)
        {
            if ((minimumLowercaseLetters + minimumUppercaseLetters + minimumNumbers + minimumSpecials) > passwordLength)
                throw new SystemException("Not a valid request! Cannot meet password requirements.");

            var randomBytes = new byte[passwordLength];
            using (var provider = new RNGCryptoServiceProvider()) provider.GetBytes(randomBytes);
            var result = new StringBuilder(passwordLength);

            var allowedCharacters = new List<char>();
            if (minimumLowercaseLetters > 0) allowedCharacters.AddRange(CharsLowercaseLetters);
            if (minimumUppercaseLetters > 0) allowedCharacters.AddRange(CharsUppercaseLetters);
            if (minimumNumbers > 0) allowedCharacters.AddRange(CharsNumbers);
            if (minimumSpecials > 0) allowedCharacters.AddRange(CharsSpecials);
            if (!allowedCharacters.Any())
                throw new SystemException("Not a valid request! There has to be some characters allowed.");
            var validCharacters = allowedCharacters.ToArray();

            var index = 0;
            while (minimumLowercaseLetters-- > 0)
                result.Append(CharsLowercaseLetters[randomBytes[index++] % CharsLowercaseLetters.Length]);
            while (minimumUppercaseLetters-- > 0)
                result.Append(CharsUppercaseLetters[randomBytes[index++] % CharsUppercaseLetters.Length]);
            while (minimumNumbers-- > 0) result.Append(CharsNumbers[randomBytes[index++] % CharsNumbers.Length]);
            while (minimumSpecials-- > 0) result.Append(CharsSpecials[randomBytes[index++] % CharsSpecials.Length]);

            for (var i = index; i < passwordLength; i++)
                result.Append(validCharacters[randomBytes[i] % validCharacters.Length]);
            var charArray = result.ToString().ToArray();
            charArray.Shuffle();
            return new string(charArray);
        }

        private static readonly Random _random = new Random(Environment.TickCount);

        private static void Shuffle<T>(this IList<T> array)
        {
            var n = array.Count;
            for (var i = 0; i < n; i++)
            {
                var r = i + _random.Next(n - i);
                var t = array[r];
                array[r] = array[i];
                array[i] = t;
            }
        }
    }
}