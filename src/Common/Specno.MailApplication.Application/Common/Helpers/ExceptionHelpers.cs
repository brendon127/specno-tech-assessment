using Specno.MailApplication.Application.Common.Exceptions;

namespace Specno.MailApplication.Application.Common.Helpers
{
    public static class ExceptionHelpers
    {
        public static void EnsureEntityWithIdExists<T, Tid>(T entity, Tid expectedId)
        {
            if (entity == null)
            {
                throw new NotFoundException($"{typeof(T).Name} with Id {expectedId} not found.");
            }
        }
    }
}