﻿using System.Reflection;
using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Specno.Domain.Entities.Conversation;
using Specno.Domain.Entities.InboxOwner;
using Specno.Domain.Entities.Message;
using Specno.Domain.Interfaces;
using Specno.MailApplication.Application.Common.Interfaces;
using Specno.MailApplication.Infrastructure.EF.Contexts;
using Specno.MailApplication.Infrastructure.EF.Repositories;
using Specno.MailApplication.Infrastructure.Services;

namespace Specno.MailApplication.Infrastructure
{
    public static class DependencyInjection
    {
        public static void AddInfrastructure(this IServiceCollection services,
            IConfiguration configuration, IWebHostEnvironment environment)
        {
            services.AddDatabase(configuration);
            services.AddScoped<IDomainEventService, DomainEventService>();
            services.AddRepositories();
            services.AddTransient<IMailDeliveryService, RabbitMqMailDeliveryService>();
            services.AddMediatR(Assembly.GetExecutingAssembly());
        }

        public static void AddInfrastructure(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddDatabase(configuration);
            services.AddScoped<IDomainEventService, DomainEventService>();
        }

        private static void AddDatabase(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(
                    configuration.GetConnectionString("DefaultConnection"),
                    b => b.MigrationsAssembly(typeof(ApplicationDbContext).Assembly.FullName)));

            services.AddTransient<IDateTime, DateTimeService>();
        }

        private static void AddRepositories(this IServiceCollection services)
        {
            services.AddTransient(typeof(IRepository<>), typeof(Repository<>));

            services.AddTransient<IMessageRepository, SqlMessageRepository>();
            services.AddTransient<IConversationRepository, SqlConversationRepository>();
            services.AddTransient<IInboxOwnerRepository, SqlInboxOwnerRepository>();
        }
    }
}