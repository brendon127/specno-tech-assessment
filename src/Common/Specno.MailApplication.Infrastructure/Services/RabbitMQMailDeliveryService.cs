using System;
using System.Linq;
using System.Text;
using System.Text.Json;
using Microsoft.Extensions.Configuration;
using RabbitMQ.Client;
using Specno.Domain.Entities.Message;
using Specno.MailApplication.Application.Common.Interfaces;
using Specno.MailExchange.Contracts;

namespace Specno.MailApplication.Infrastructure.Services
{
    public class RabbitMqMailDeliveryService : IMailDeliveryService
    {
        private readonly IConfiguration _configuration;

        public RabbitMqMailDeliveryService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void Deliver(Message message)
        {
            var factory = new ConnectionFactory()
            {
                Uri = new Uri(_configuration["RabbitMQ:DefaultConnection"])
            };
            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();
            channel.QueueDeclare("email.exchange",
                durable: true,
                exclusive: false,
                autoDelete: false,
                arguments: null);
            var mess = MapMessage(message);
            var body = SerializeMessage(mess);
            channel.BasicPublish("", "email.exchange", null, body);
        }
        private static byte[] SerializeMessage(EmailMessage mess)
        {
            var serializedMessage = JsonSerializer.Serialize(mess);
            var body = Encoding.UTF8.GetBytes(serializedMessage);
            return body;
        }

        private static EmailMessage MapMessage(Message message)
        {
            return new EmailMessage
            {
                Subject = message.Subject,
                Body = message.Body,
                Sender = message.Sender.Address,

                ReplyTo = message.ReplyTo.Address,
                ReplyTos = message.ReplyTos.Select(s => s.Address),

                ToList = message.ToList.Select(s => s.Address),
                CcList = message.CcList.Select(s => s.Address),
                BccList = message.BccList.Select(s => s.Address),
            };
        }

        private static IModel GetChannel()
        {
            var factory = new ConnectionFactory
            {
                Uri = new Uri("amqp://guest:guest@localhost:5672")
            };
            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();
            channel.QueueDeclare("email.exchange",
                durable: true,
                exclusive: false,
                autoDelete: false,
                arguments: null);
            return channel;
        }
    }
}