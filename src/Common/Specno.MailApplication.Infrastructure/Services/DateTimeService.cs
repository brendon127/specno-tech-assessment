﻿using System;
using Specno.MailApplication.Application.Common.Interfaces;

namespace Specno.MailApplication.Infrastructure.Services
{
    public class DateTimeService : IDateTime
    {
        public DateTime Now => DateTime.UtcNow;
    }
}
