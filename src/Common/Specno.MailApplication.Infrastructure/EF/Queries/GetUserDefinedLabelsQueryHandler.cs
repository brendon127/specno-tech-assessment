using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Specno.Domain.Entities.InboxOwner;
using Specno.MailApplication.Application.Common.Interfaces;
using Specno.MailApplication.Application.Common.Models;
using Specno.MailApplication.Application.Labels.Queries;

namespace Specno.MailApplication.Infrastructure.EF.Queries
{
    public class
        GetUserDefinedLabelsQueryHandler : IRequestHandlerWrapper<GetUserDefinedLabelsQuery, IEnumerable<UserDefinedLabelDto>>
    {
        private readonly IInboxOwnerRepository _inboxOwnerRepository;

        public GetUserDefinedLabelsQueryHandler(IInboxOwnerRepository inboxOwnerRepository)
        {
            _inboxOwnerRepository = inboxOwnerRepository;
        }

        public Task<ServiceResult<IEnumerable<UserDefinedLabelDto>>> Handle(GetUserDefinedLabelsQuery request,
            CancellationToken cancellationToken)
        {
            var inboxOwner = _inboxOwnerRepository.GetCurrentInboxOwner();
            var yo = inboxOwner.UserDefinedLabels.Select(label => new UserDefinedLabelDto()
            {
                Id = label.Id,
                Name = label.Name
            });
            return Task.FromResult(ServiceResult.Success(yo));
        }
    }
}