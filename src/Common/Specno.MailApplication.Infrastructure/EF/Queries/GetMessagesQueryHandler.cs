using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Specno.Domain.Entities.InboxOwner;
using Specno.Domain.Entities.Message;
using Specno.MailApplication.Application.Common.Interfaces;
using Specno.MailApplication.Application.Common.Models;
using Specno.MailApplication.Application.Message.Queries;

namespace Specno.MailApplication.Infrastructure.EF.Queries
{
    public class GetMessagesQueryHandler : IRequestHandlerWrapper<GetMessagesQuery, IEnumerable<MessageDto>>
    {
        private readonly IInboxOwnerRepository _inboxOwnerRepository;

        public GetMessagesQueryHandler(IInboxOwnerRepository inboxOwnerRepository)
        {
            _inboxOwnerRepository = inboxOwnerRepository;
        }

        public Task<ServiceResult<IEnumerable<MessageDto>>> Handle(GetMessagesQuery request,
            CancellationToken cancellationToken)
        {
            var inboxOwner = _inboxOwnerRepository.GetCurrentInboxOwner();

            var labelId = request.LabelId;

            IEnumerable<Message> messages = null;
            if (Guid.TryParse(labelId, out var labelIdGuid))
            {
                messages = inboxOwner.Conversations
                    .SelectMany(s => s.Messages)
                    .Where(m => m.UserDefinedLabelIds.Contains(labelIdGuid))
                    .ToList();
            }
            else if (int.TryParse(labelId, out var labelIdInt))
            {
                messages = inboxOwner.Conversations
                    .SelectMany(s => s.Messages)
                    .Where(s => s.SystemDefinedLabelIds.Contains(labelIdInt))
                    .ToList();
            }
            else
            {
                messages = inboxOwner.Conversations
                    .SelectMany(s => s.Messages)
                    .ToList();
            }

            var messageDtos = messages.Select(m => new MessageDto
            {
                Id = m.Id,
                Body = m.Body,
                Subject = m.Subject,
                ConversationId = m.ConversationId,
                ToList = m.ToList,
                CcList = m.CcList,
                BccList = m.BccList,
            });

            return Task.FromResult(ServiceResult.Success<IEnumerable<MessageDto>>(messageDtos));
        }
    }
}