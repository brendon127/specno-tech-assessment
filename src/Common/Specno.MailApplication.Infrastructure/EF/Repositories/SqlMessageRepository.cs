using System.Collections.Generic;
using System.Linq;
using Specno.Domain.Entities.InboxOwner;
using Specno.Domain.Entities.Message;
using Specno.MailApplication.Infrastructure.EF.Contexts;

namespace Specno.MailApplication.Infrastructure.EF.Repositories
{
    public class SqlMessageRepository : Repository<Message>, IMessageRepository
    {
        public SqlMessageRepository(ApplicationDbContext context) : base(context)
        {
        }

        public IEnumerable<Message> GetInboxOwnerMessages(InboxOwner inboxOwner)
        {
            return inboxOwner.Conversations.SelectMany(s => s.Messages);
        }
    }
}