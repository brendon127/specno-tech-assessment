using Specno.Domain.Entities.Conversation;
using Specno.MailApplication.Infrastructure.EF.Contexts;

namespace Specno.MailApplication.Infrastructure.EF.Repositories
{
    public class SqlConversationRepository : Repository<Conversation>, IConversationRepository
    {
        public SqlConversationRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}