using System.Linq;
using Specno.Domain.Entities.InboxOwner;
using Specno.MailApplication.Application.Common.Interfaces;
using Specno.MailApplication.Infrastructure.EF.Contexts;

namespace Specno.MailApplication.Infrastructure.EF.Repositories
{
    public class SqlInboxOwnerRepository : Repository<InboxOwner>, IInboxOwnerRepository
    {
        private readonly ICurrentUserService _currentUserService;

        public SqlInboxOwnerRepository(ApplicationDbContext context, ICurrentUserService currentUserService) :
            base(context)
        {
            _currentUserService = currentUserService;
        }

        public InboxOwner GetCurrentInboxOwner()
        {
            var inboxOwner = GetAll()
                .FirstOrDefault(inboxOwner => inboxOwner.UserId == _currentUserService.UserId);
            return inboxOwner;
        }
    }
}