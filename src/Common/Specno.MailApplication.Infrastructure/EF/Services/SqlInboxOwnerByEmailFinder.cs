using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Specno.Domain.Entities.InboxOwner;
using Specno.MailApplication.Infrastructure.EF.Contexts;

namespace Specno.MailApplication.Infrastructure.EF.Services
{
    public class SqlInboxOwnerByEmailFinder : IInboxOwnerByEmailFinder
    {
        private readonly ApplicationDbContext _applicationDbContext;

        public SqlInboxOwnerByEmailFinder(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public Task<InboxOwner> FindAsync(string email)
        {
            return _applicationDbContext
                .InboxOwners
                .AsSingleQuery()
                .FirstOrDefaultAsync(s =>
                    s.EmailAddresses.Select(ownerEmailAddress => ownerEmailAddress.EmailAddress).Contains(email));
        }
    }
}