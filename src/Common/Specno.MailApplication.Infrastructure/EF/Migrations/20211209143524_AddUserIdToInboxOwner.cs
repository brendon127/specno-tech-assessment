﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Armo.Infrastructure.Persistence.Migrations
{
    public partial class AddUserIdToInboxOwner : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "UserId",
                table: "InboxOwner",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserId",
                table: "InboxOwner");
        }
    }
}
