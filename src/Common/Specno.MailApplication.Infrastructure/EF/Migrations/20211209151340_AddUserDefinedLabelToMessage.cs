﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Armo.Infrastructure.Persistence.Migrations
{
    public partial class AddUserDefinedLabelToMessage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "UserDefinedMessageLabel",
                columns: table => new
                {
                    MessageId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    UserDefinedLabelId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserDefinedMessageLabel", x => new { x.MessageId, x.UserDefinedLabelId });
                    table.ForeignKey(
                        name: "FK_UserDefinedMessageLabel_Messages_MessageId",
                        column: x => x.MessageId,
                        principalTable: "Messages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserDefinedMessageLabel_UserDefinedLabel_UserDefinedLabelId",
                        column: x => x.UserDefinedLabelId,
                        principalTable: "UserDefinedLabel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserDefinedMessageLabel_UserDefinedLabelId",
                table: "UserDefinedMessageLabel",
                column: "UserDefinedLabelId");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserDefinedLabel_Messages_MessageId",
                table: "UserDefinedLabel");

            migrationBuilder.DropTable(
                name: "UserDefinedMessageLabel");
        }
    }
}
