﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Armo.Infrastructure.Persistence.Migrations
{
    public partial class AddInboxUserToConversation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "InboxOwnerId",
                table: "Conversations",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));


            migrationBuilder.AddForeignKey(
                name: "FK_Conversations_InboxOwner_InboxOwnerId",
                table: "Conversations",
                column: "InboxOwnerId",
                principalTable: "InboxOwner",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetDefault);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Conversations_InboxOwner_InboxOwnerId",
                table: "Conversations");

            migrationBuilder.DropIndex(
                name: "IX_Conversations_InboxOwnerId",
                table: "Conversations");

            migrationBuilder.DropColumn(
                name: "InboxOwnerId",
                table: "Conversations");
        }
    }
}
