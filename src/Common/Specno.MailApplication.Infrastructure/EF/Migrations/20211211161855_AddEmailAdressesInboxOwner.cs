﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Armo.Infrastructure.Persistence.Migrations
{
    public partial class AddEmailAdressesInboxOwner : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "InboxOwnerEmailAddress",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    EmailAddress = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    InboxOwnerId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InboxOwnerEmailAddress", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InboxOwnerEmailAddress_InboxOwner_InboxOwnerId",
                        column: x => x.InboxOwnerId,
                        principalTable: "InboxOwner",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_InboxOwnerEmailAddress_InboxOwnerId",
                table: "InboxOwnerEmailAddress",
                column: "InboxOwnerId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InboxOwnerEmailAddress");
        }
    }
}
