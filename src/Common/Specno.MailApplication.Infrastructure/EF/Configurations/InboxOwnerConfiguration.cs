using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Specno.Domain.Entities.InboxOwner;

namespace Specno.MailApplication.Infrastructure.EF.Configurations
{
    public class InboxOwnerConfiguration : IEntityTypeConfiguration<InboxOwner>
    {
        public void Configure(EntityTypeBuilder<InboxOwner> builder)
        {
            builder.ToTable("InboxOwner");
            builder.Property(i => i.Id)
                .ValueGeneratedNever();

            builder.HasMany(s => s.EmailAddresses)
                .WithOne()
                .HasForeignKey(s => s.InboxOwnerId);

            builder.HasMany(s => s.UserDefinedLabels)
                .WithOne()
                .HasForeignKey(s => s.InboxOwnerId);

            builder.Navigation(s => s.UserDefinedLabels)
                .AutoInclude();

            builder.Property(i => i.UserId);

            builder.Navigation(s => s.EmailAddresses)
                .AutoInclude();

            builder.Navigation(s => s.Conversations)
                .AutoInclude();

            builder.HasMany(s => s.Conversations)
                .WithOne()
                .HasForeignKey(s => s.InboxOwnerId);

            builder.Ignore(s => s.DomainEvents);
        }
    }
}