using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Specno.Domain.Entities.Conversation;
using Specno.Domain.Entities.Message;
using Specno.MailApplication.Infrastructure.EF.Extensions;

namespace Specno.MailApplication.Infrastructure.EF.Configurations
{
    public class MessageConfiguration : IEntityTypeConfiguration<Message>
    {
        public void Configure(EntityTypeBuilder<Message> builder)
        {
            builder.ToTable("Messages");

            builder.Property(p => p.Id)
                .ValueGeneratedNever();

            builder.Property(m => m.Subject);
            builder.Property(m => m.Body);

            builder.HasOne<Conversation>()
                .WithMany(s => s.Messages)
                .HasForeignKey(m => m.ConversationId);

            builder.OwnsOne(s => s.ReplyTo, navigationBuilder =>
            {
                navigationBuilder.Property(s => s.Address)
                    .HasColumnName("ReplyToAddress");
                navigationBuilder.Property(s => s.Name)
                    .HasColumnName("ReplyToName");
            });


            builder.Navigation(s => s.MessageSystemDefinedLabels)
                .AutoInclude();

            builder.HasMany(s => s.UserDefinedLabels)
                .WithMany(s => s.Messages)
                .UsingEntity<UserDefinedMessageLabel>(
                    ud =>
                        ud.HasOne(s => s.UserDefinedLabel)
                            .WithMany()
                            .HasForeignKey(s => s.UserDefinedLabelId),
                    ad =>
                        ad.HasOne(s => s.Message)
                            .WithMany(s => s.UserDefinedMessageLabels)
                            .HasForeignKey(s => s.MessageId)
                ).HasKey(bg => new {bg.MessageId, bg.UserDefinedLabelId});

            builder.Navigation(s => s.UserDefinedLabels)
                .AutoInclude();

            // builder.Navigation(s => s.UserDefinedMessageLabels)
            //     .AutoInclude();

            builder.OwnsOne(s => s.Sender, navigationBuilder =>
            {
                navigationBuilder.Property(s => s.Address)
                    .HasColumnName("SenderAddress");
                navigationBuilder.Property(s => s.Name)
                    .HasColumnName("SenderName");
            });

            builder.Property(m => m.ToList)
                .HasJsonConversion();

            builder.Property(m => m.CcList)
                .HasJsonConversion();

            builder.Property(m => m.BccList)
                .HasJsonConversion();

            builder.Property(m => m.ReplyTos)
                .HasJsonConversion();
        }
    }
}