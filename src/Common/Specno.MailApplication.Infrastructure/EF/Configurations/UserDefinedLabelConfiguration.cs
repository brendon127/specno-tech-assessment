using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Specno.Domain.Entities.Label;

namespace Specno.MailApplication.Infrastructure.EF.Configurations
{
    public class UserDefinedLabelConfiguration : IEntityTypeConfiguration<UserDefinedLabel>
    {
        public void Configure(EntityTypeBuilder<UserDefinedLabel> builder)
        {
            builder.ToTable("UserDefinedLabel");
            builder.Property(l => l.Id)
                .ValueGeneratedNever();

            builder.Property(l => l.Name);
        }
    }
}