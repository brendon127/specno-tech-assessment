using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Specno.Domain.Entities.InboxOwner;

namespace Specno.MailApplication.Infrastructure.EF.Configurations
{
    public class InboxOwnerEmailAddressConfiguration : IEntityTypeConfiguration<InboxOwner.InboxOwnerEmailAddress>
    {
        public void Configure(EntityTypeBuilder<InboxOwner.InboxOwnerEmailAddress> builder)
        {
            builder.Property(o => o.Id)
                .ValueGeneratedNever();
            builder.Property(s => s.EmailAddress);
        }
    }
}