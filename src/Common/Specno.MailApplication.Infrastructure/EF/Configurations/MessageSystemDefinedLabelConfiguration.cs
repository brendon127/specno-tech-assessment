using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Specno.Domain.Entities.Label;

namespace Specno.MailApplication.Infrastructure.EF.Configurations
{
    public class MessageSystemDefinedLabelConfiguration : IEntityTypeConfiguration<SystemDefinedMessageLabel>
    {
        public void Configure(EntityTypeBuilder<SystemDefinedMessageLabel> builder)
        {
            builder.ToTable("MessageSystemDefinedLabelss");
            
            builder
                .HasKey(bc => new {bc.MessageId, bc.SystemDefinedLabelId});
        }
    }
}