using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Specno.Domain.Entities.Conversation;

namespace Specno.MailApplication.Infrastructure.EF.Configurations
{
    public class ConversationConfiguration : IEntityTypeConfiguration<Conversation>
    {
        public void Configure(EntityTypeBuilder<Conversation> builder)
        {
            builder.ToTable("Conversations");

            builder.Property(a => a.Id)
                .ValueGeneratedNever();

            builder.Navigation(s => s.Messages)
                .AutoInclude();

            builder.HasMany(s => s.Messages)
                .WithOne()
                .HasForeignKey(s => s.ConversationId);
            
            builder.Property(a => a.Subject);
        }
    }
}