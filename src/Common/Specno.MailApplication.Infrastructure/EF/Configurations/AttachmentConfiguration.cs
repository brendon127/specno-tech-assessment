using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Specno.Domain.Entities.Attachments;

namespace Specno.MailApplication.Infrastructure.EF.Configurations
{
    public class AttachmentConfiguration : IEntityTypeConfiguration<Attachment>
    {
        public void Configure(EntityTypeBuilder<Attachment> builder)
        {
            builder.ToTable("Attachments");

            builder.Property(a => a.Id)
                .ValueGeneratedOnAdd();

            builder.Property(a => a.Key)
                .IsRequired();

            builder.Property(a => a.ContentType)
                .HasMaxLength(32)
                .IsRequired();

            builder.Property(a => a.ByteSize)
                .IsRequired();
        }
    }
}