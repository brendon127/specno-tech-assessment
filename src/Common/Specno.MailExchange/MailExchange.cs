﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using RabbitMQ.Client;
using Specno.MailExchange.Contracts;

namespace Specno.MailExchange
{
    public class MailExchange : IMailExchange
    {
        private readonly IConfiguration _configuration;
        private EmailMessage _message;

        public MailExchange(IConfiguration configuration)
        {
            _configuration = configuration;
        }


        public async Task Process(EmailMessage message)
        {
            _message = message;
            var toAddresses = message.ToList
                .Concat(message.CcList)
                .Concat(message.BccList)
                .Distinct();
            foreach (var address in toAddresses)
            {
                await DeliverEmail(address);
            }
        }

        private async Task DeliverEmail(string address)
        {
            // Similar to a DNS lookup in real email system.
            if (!await ToAddressExists(address))
            {
                await ReturnMessageToSender();
                return;
            }

            _message.BccList = new List<string>();
            _message.Recipient = address;

            var connectionString = _configuration["RabbitMQ:DefaultConnection"];
            var factory = new ConnectionFactory
            {
                Uri = new Uri(connectionString)
            };
            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();
            channel.QueueDeclare("email.delivery",
                durable: true,
                exclusive: false,
                autoDelete: false,
                arguments: null);
            var body = SerializeMessage(_message);
            channel.BasicPublish("", "email.delivery", null, body);
        }

        private static byte[] SerializeMessage(EmailMessage mess)
        {
            var serializedMessage = JsonSerializer.Serialize(mess);
            var body = Encoding.UTF8.GetBytes(serializedMessage);
            return body;
        }

        private Task ReturnMessageToSender()
        {
            // Drop message
            var message = _message;
            return Task.CompletedTask;
        }

        private async Task<bool> ToAddressExists(string address)
        {
            return await Task.FromResult(true);
        }
    }
}