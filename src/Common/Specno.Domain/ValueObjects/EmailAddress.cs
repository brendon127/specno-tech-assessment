using System.Collections.Generic;
using Specno.Domain.Common;

namespace Specno.Domain.ValueObjects
{
    public class EmailAddress : ValueObject
    {
        private EmailAddress()
        {
            
        }
        public EmailAddress(string address, string name)
        {
            Address = address;
            Name = name;
        }

        public string Address { get; }
        public string Name { get; }
        
        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return Address;
            yield return Name;
        }
    }
}