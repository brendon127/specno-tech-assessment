using System;

namespace Specno.Domain.Interfaces
{
    public interface ISoftDeletable
    {
        public string DeletedBy { get; set; }
        public DateTime ? DeletedAt { get; set; }
    }
}