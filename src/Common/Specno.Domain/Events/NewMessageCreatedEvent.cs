using Specno.Domain.Common;
using Specno.Domain.Entities.Message;

namespace Specno.Domain.Events
{
    public class NewMessageCreatedEvent : DomainEvent
    {
        public Message Message { get; }
        public NewMessageCreatedEvent(Message message)
        {
            Message = message;
        }
    }
}