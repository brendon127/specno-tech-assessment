using Microsoft.Extensions.DependencyInjection;
using Specno.Domain.Entities.Message;

namespace Specno.Domain
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddDomain(this IServiceCollection services)
        {
            services.AddTransient<MessageBuilder>();
            return services;
        }
    }
}