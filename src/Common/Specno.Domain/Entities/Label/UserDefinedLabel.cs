using System;
using System.Collections.Generic;
using Specno.Domain.Common;

namespace Specno.Domain.Entities.Label
{
    public class UserDefinedLabel : AuditableEntity
    {
        public Guid Id { get; set; }
        public Guid InboxOwnerId { get; set; }
        public string Name { get; set; }
        public IEnumerable<Message.Message> Messages { get; set; }

        public UserDefinedLabel(string name)
        {
            Id = Guid.NewGuid();
            Name = name;
        }
    }
}