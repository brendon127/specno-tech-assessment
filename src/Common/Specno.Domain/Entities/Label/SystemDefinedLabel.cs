using Ardalis.SmartEnum;

namespace Specno.Domain.Entities.Label
{
    public class SystemDefinedLabel : SmartEnum<SystemDefinedLabel>
    {
        public static readonly SystemDefinedLabel Inbox = new("Inbox", 1);
        public static readonly SystemDefinedLabel Draft = new("Draft", 2);
        public static readonly SystemDefinedLabel Sent = new("Sent", 3);
        public static readonly SystemDefinedLabel Starred = new("Starred", 4);
        public static readonly SystemDefinedLabel Archive = new("Archive", 5);
        public static readonly SystemDefinedLabel Spam = new("Spam", 6);
        public static readonly SystemDefinedLabel Trash = new("Trash", 7);
        public static readonly SystemDefinedLabel AllMail = new("AllMail", 8);

        // Ef core don't use
        private SystemDefinedLabel() : base("EfCore", -1)
        {
        }

        private SystemDefinedLabel(string name, int value) : base(name, value)
        {
        }
    }
}