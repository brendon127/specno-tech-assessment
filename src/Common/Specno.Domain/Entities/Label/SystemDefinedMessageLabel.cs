using System;

namespace Specno.Domain.Entities.Label
{
    public class SystemDefinedMessageLabel
    {
        public Guid MessageId { get; set; }
        public int SystemDefinedLabelId { get; set; }
    }
}