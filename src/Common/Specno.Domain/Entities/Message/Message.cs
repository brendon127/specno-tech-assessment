using System;
using System.Collections.Generic;
using System.Linq;
using Specno.Domain.Common;
using Specno.Domain.Entities.Label;
using Specno.Domain.ValueObjects;

namespace Specno.Domain.Entities.Message
{
    public class Message : AuditableEntity
    {
        public Guid Id { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public Guid ConversationId { get; internal set; }
        public List<EmailAddress> ReplyTos { get; set; } // make email a value object
        public EmailAddress ReplyTo { get; set; }
        public EmailAddress Sender { get; set; }
        public List<EmailAddress> ToList { get; set; }
        public List<EmailAddress> CcList { get; set; }
        public List<EmailAddress> BccList { get; set; }
        public IEnumerable<int> SystemDefinedLabelIds => MessageSystemDefinedLabels.Select(s => s.SystemDefinedLabelId);

        public ICollection<SystemDefinedMessageLabel> MessageSystemDefinedLabels { get; set; } =
            new List<SystemDefinedMessageLabel>();

        public List<UserDefinedLabel> UserDefinedLabels { get; set; } = new();

        public List<Guid> UserDefinedLabelIds => UserDefinedLabels.Select(s => s.Id).ToList();

        public ICollection<UserDefinedMessageLabel> UserDefinedMessageLabels { get; set; } =
            new List<UserDefinedMessageLabel>();


        // Ef core
        private Message()
        {
        }

        internal Message(string subject, string body, Guid conversationId, List<EmailAddress> replyTos,
            EmailAddress replyTo, EmailAddress sender, List<EmailAddress> toList,
            List<EmailAddress> ccList, List<EmailAddress> bccList)
        {
            Id = Guid.NewGuid();
            Subject = subject;
            Body = body;
            ConversationId = conversationId;
            ReplyTos = replyTos;
            ReplyTo = replyTo;
            Sender = sender;
            ToList = toList;
            CcList = ccList;
            BccList = bccList;
        }

        internal Message(MessageBuilder messageBuilder)
        {
            Id = Guid.NewGuid();

            BccList = messageBuilder.MessageRecipients.BccList;
            CcList = messageBuilder.MessageRecipients.CcList;
            ToList = messageBuilder.MessageRecipients.ToList;

            ReplyTo = messageBuilder.ReplyRecipients.ReplyTo;
            ReplyTos = messageBuilder.ReplyRecipients.ReplyTos;

            Sender = messageBuilder.Sender;

            Subject = messageBuilder.Subject;
            Body = messageBuilder.Body;
        }

        public void AddLabel(UserDefinedLabel userDefinedLabel)
        {
            UserDefinedLabels.Add(userDefinedLabel);
        }

        public void RemoveLabel(UserDefinedLabel userDefinedLabel)
        {
            UserDefinedLabels.Remove(userDefinedLabel);
        }

        public void AddLabel(SystemDefinedLabel systemDefinedLabel)
        {
            if (MessageSystemDefinedLabels.All(s => s.SystemDefinedLabelId != systemDefinedLabel.Value))
            {
                MessageSystemDefinedLabels.Add(new SystemDefinedMessageLabel
                {
                    SystemDefinedLabelId = systemDefinedLabel.Value,
                    MessageId = Id,
                });
            }
        }

        public void MoveToTrash()
        {
            MessageSystemDefinedLabels.Clear();
            AddLabel(SystemDefinedLabel.Trash);
            AddLabel(SystemDefinedLabel.AllMail);
        }

        public void Recover()
        {
            MessageSystemDefinedLabels.Clear();
            AddLabel(SystemDefinedLabel.Inbox);
            AddLabel(SystemDefinedLabel.AllMail);
        }
    }
}