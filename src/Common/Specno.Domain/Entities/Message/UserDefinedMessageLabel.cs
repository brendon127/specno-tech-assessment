using System;
using Specno.Domain.Entities.Label;

namespace Specno.Domain.Entities.Message
{
    public class UserDefinedMessageLabel
    {
        public Message Message { get; set; }
        public Guid MessageId { get; set; }
        public Guid UserDefinedLabelId { get; set; }
        public UserDefinedLabel UserDefinedLabel { get; set; }
    }
}