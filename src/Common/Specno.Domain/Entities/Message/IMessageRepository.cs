using System.Collections.Generic;
using Specno.Domain.Interfaces;

namespace Specno.Domain.Entities.Message
{
    public interface IMessageRepository : IRepository<Message>
    {
        public IEnumerable<Message> GetInboxOwnerMessages(InboxOwner.InboxOwner inboxOwner);
    }
}