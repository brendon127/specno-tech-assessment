using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Specno.Domain.Entities.Label;
using Specno.Domain.ValueObjects;

namespace Specno.Domain.Entities.Message
{
    public class MessageBuilder
    {
        internal Guid ConversationId { get; private set; }
        internal MessageRecipients MessageRecipients { get; private set; }
        internal ReplyRecipients ReplyRecipients { get; private set; }
        internal string Body { get; private set; }
        internal string Subject { get; private set; }
        internal EmailAddress Sender { get; set; }
        protected internal ICollection<SystemDefinedLabel> SystemDefinedLabels { get; protected set; }

        public MessageBuilder()
        {
            MessageRecipients = new MessageRecipients();
            ReplyRecipients = new ReplyRecipients();
        }


        public MessageBuilder WithRecipients(MessageRecipients messageRecipients)
        {
            MessageRecipients = messageRecipients;
            return this;
        }

        public MessageBuilder WithReplyRecipients(ReplyRecipients replyRecipients)
        {
            ReplyRecipients = replyRecipients;
            return this;
        }

        public MessageBuilder WithSender(EmailAddress sender)
        {
            Sender = sender;
            return this;
        }

        public MessageBuilder WithSubject(string subject)
        {
            Subject = subject;
            return this;
        }

        public MessageBuilder WithBody(string body)
        {
            Body = body;
            return this;
        }

        public Task<Message> BuildAsync()
        {
            // SetConversation();
            return Task.FromResult(new Message(this));
        }
    }

    public class ReplyRecipients
    {
        public List<EmailAddress> ReplyTos { get; set; }
        public EmailAddress ReplyTo { get; set; }
    }


    public class MessageRecipients
    {
        public List<EmailAddress> BccList { get; set; }
        public List<EmailAddress> ToList { get; set; }
        public List<EmailAddress> CcList { get; set; }
    }
}