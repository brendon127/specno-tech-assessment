using Specno.Domain.Interfaces;

namespace Specno.Domain.Entities.Conversation
{
    public interface IConversationRepository : IRepository<Conversation>
    {
        
    }
}