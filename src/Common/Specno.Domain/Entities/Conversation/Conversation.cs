using System;
using System.Collections.Generic;
using Specno.Domain.Common;

namespace Specno.Domain.Entities.Conversation
{
    public class Conversation : AuditableEntity
    {
        public Guid Id { get; }
        public string Subject { get; }
        public Guid InboxOwnerId { get; set; }
        public ICollection<Message.Message> Messages { get; set; } = new List<Message.Message>();

        // Add calculated fields

        private Conversation()
        {
        }

        public Conversation(string subject)
        {
            Id = Guid.NewGuid();
            Subject = subject;
        }

        public void AddMessage(Message.Message message)
        {
            message.ConversationId = Id;
            Messages.Add(message);
        }
    }
}