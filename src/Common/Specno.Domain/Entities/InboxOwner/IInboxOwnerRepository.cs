using Specno.Domain.Interfaces;

namespace Specno.Domain.Entities.InboxOwner
{
    public interface IInboxOwnerRepository : IRepository<InboxOwner>
    {
        public InboxOwner GetCurrentInboxOwner();

    }
}