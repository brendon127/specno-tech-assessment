using System.Threading.Tasks;

namespace Specno.Domain.Entities.InboxOwner
{
    public interface IInboxOwnerByEmailFinder
    {
        public Task<InboxOwner> FindAsync(string email);
    }
}