using System;
using System.Collections.Generic;
using System.Linq;
using Specno.Domain.Common;
using Specno.Domain.Entities.Label;
using Specno.Domain.Events;
using Specno.Domain.Exceptions;

namespace Specno.Domain.Entities.InboxOwner
{
    public class InboxOwner : AuditableEntity, IHasDomainEvent
    {
        public Guid Id { get; }
        public string UserId { get; }

        public ICollection<InboxOwnerEmailAddress> EmailAddresses { get; private set; } =
            new List<InboxOwnerEmailAddress>();

        public ICollection<UserDefinedLabel> UserDefinedLabels { get; } = new List<UserDefinedLabel>();

        public ICollection<Conversation.Conversation> Conversations { get; set; } =
            new List<Conversation.Conversation>();

        public List<DomainEvent> DomainEvents { get; set; } = new();

        private InboxOwner()
        {
        }

        public InboxOwner(string userId)
        {
            Id = Guid.NewGuid();
            UserId = userId;
        }

        public void AddEmailAddress(string newEmailAddress)
        {
            if (EmailAddresses.All(a => a.EmailAddress != newEmailAddress))
            {
                EmailAddresses.Add(new InboxOwnerEmailAddress(newEmailAddress));
            }
        }

        public void SendMessage(Message.Message newMessage)
        {
            CanSendMessage(newMessage);

            var conversation = FindOrCreateConversationBySubject(newMessage.Subject);
            newMessage.AddLabel(SystemDefinedLabel.Sent);
            newMessage.AddLabel(SystemDefinedLabel.AllMail);
            conversation.AddMessage(newMessage);

            DomainEvents.Add(new NewMessageCreatedEvent(newMessage));
        }

        // Could possibly add a classifier to determine spam and such
        public void ReceiveMessage(Message.Message newMessage)
        {
            var conversation = FindOrCreateConversationBySubject(newMessage.Subject);
            newMessage.AddLabel(SystemDefinedLabel.Inbox);
            newMessage.AddLabel(SystemDefinedLabel.AllMail);
            conversation.AddMessage(newMessage);
        }

        public void CreateLabel(UserDefinedLabel label)
        {
            // Only allow unique label here... maybe
            UserDefinedLabels.Add(label);
        }

        public void DeleteLabel(Guid labelId)
        {
            var toRemove = UserDefinedLabels
                .FirstOrDefault(s => s.Id == labelId);
            if (toRemove is null)
            {
                return;
            }

            UserDefinedLabels.Remove(toRemove);
        }

        private void CanSendMessage(Message.Message newMessage)
        {
            if (EmailAddresses.Any(s => s.EmailAddress != newMessage.Sender.Address))
            {
                throw new DomainException("User does not own Sender email address");
            }
        }

        private Conversation.Conversation FindOrCreateConversationBySubject(string subject)
        {
            var conversation = Conversations.FirstOrDefault(c => c.Subject == subject);
            if (conversation != null)
            {
                return conversation;
            }

            conversation = new Conversation.Conversation(subject);
            Conversations.Add(conversation);
            return conversation;
        }


        public void MoveMessageToTrash(Guid messageId)
        {
            var message = Conversations
                .SelectMany(s => s.Messages)
                .FirstOrDefault(s => s.Id == messageId);
            if (message == null)
            {
                throw new DomainException("Message not found");
            }

            message.MoveToTrash();
        }

        public void RecoverMessage(Guid messageId)
        {
            var message = Conversations
                .SelectMany(s => s.Messages)
                .FirstOrDefault(s => s.Id == messageId);
            if (message == null)
            {
                throw new DomainException("Message not found");
            }

            message.Recover();
        }

        public void AddLabelToMessage(Guid labelId, Guid messageId)
        {
            var message = Conversations.SelectMany(s => s.Messages)
                .FirstOrDefault(m => m.Id == messageId);

            var label = UserDefinedLabels.FirstOrDefault(s => s.Id == labelId);

            if (message == null || label == null)
            {
                throw new DomainException("Message or label does not exist");
            }

            message.AddLabel(label);
        }

        public class InboxOwnerEmailAddress
        {
            public Guid Id { get; }
            public string EmailAddress { get; }
            public Guid InboxOwnerId { get; private set; }

            public InboxOwnerEmailAddress(string emailAddress)
            {
                Id = Guid.NewGuid();
                EmailAddress = emailAddress;
            }
        }

        public void RemoveLabelFromMessage(Guid labelId, Guid messageId)
        {
            var message = Conversations.SelectMany(s => s.Messages)
                .FirstOrDefault(s => s.Id == messageId);

            var label = UserDefinedLabels.FirstOrDefault(s => s.Id == labelId);

            if (message == null || label == null)
            {
                throw new DomainException("Message or label does not exist");
            }

            message.RemoveLabel(label);
        }
    }
}