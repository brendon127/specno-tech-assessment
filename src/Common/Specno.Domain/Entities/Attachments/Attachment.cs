using System;

namespace Specno.Domain.Entities.Attachments
{
    public class Attachment
    {
        public Guid Id { get; private set; }
        public string Key { get; private set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public long ByteSize { get; set; }

        private Attachment()
        {
            Key = Guid.NewGuid().ToString();
        }

        public Attachment(string contentType, long byteSize) : this()
        {
            ContentType = contentType;
            ByteSize = byteSize;
        }
    }
}