﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Specno.Domain.Entities.Message;
using Specno.Domain.ValueObjects;
using Specno.MailApplication.Application.Common.Interfaces;
using Specno.MailApplication.Infrastructure.EF.Contexts;
using Specno.MailApplication.Infrastructure.EF.Repositories;
using Specno.MailApplication.Infrastructure.EF.Services;
using Specno.MailExchange.Contracts;

namespace Specno.MailDelivery
{
    public interface IIncomingEmailService
    {
        public Task Deliver(EmailMessage incomingMessage);
    }

    public class IncomingEmailService : IIncomingEmailService
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly MessageBuilder _messageBuilder;
        private readonly ICurrentUserService _currentUserService;
        private readonly ILogger<IncomingEmailService> _logger;

        public IncomingEmailService(IServiceProvider serviceProvider, MessageBuilder messageBuilder,
            ICurrentUserService currentUserService, ILogger<IncomingEmailService> logger)
        {
            _serviceProvider = serviceProvider;
            _messageBuilder = messageBuilder;
            _currentUserService = currentUserService;
            _logger = logger;
        }

        public async Task Deliver(EmailMessage incomingMessage)
        {
            using var scope = _serviceProvider.CreateScope();
            var dbContext = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();

            var inboxOwnerByEmailFinder = new SqlInboxOwnerByEmailFinder(dbContext);
            var inboxOwner =
                await inboxOwnerByEmailFinder.FindAsync(incomingMessage.Recipient); 
            var inboxOwnerRepo = new SqlInboxOwnerRepository(dbContext, _currentUserService);

            var m = await BuildMessage(incomingMessage);
            inboxOwner.ReceiveMessage(m);
            await inboxOwnerRepo.UpdateAsync(inboxOwner);
            _logger.LogInformation($"Message to {incomingMessage.Recipient} delivered");
        }

        private async Task<Message> BuildMessage(EmailMessage message)
        {
            var m = await _messageBuilder
                .WithSender(new EmailAddress(message.Sender, message.Sender))
                .WithSubject(message.Subject)
                .WithBody(message.Body)
                .WithRecipients(new MessageRecipients
                {
                    ToList = message.ToList.Select(address => new EmailAddress(address, address)).ToList(),
                    CcList = message.CcList.Select(address => new EmailAddress(address, address)).ToList(),
                    BccList = message.BccList.Select(address => new EmailAddress(address, address)).ToList(),
                })
                .WithReplyRecipients(new ReplyRecipients
                {
                    ReplyTo = new EmailAddress(message.ReplyTo, message.ReplyTo),
                    ReplyTos = message.ReplyTos.Select(address => new EmailAddress(address, address)).ToList(),
                })
                .BuildAsync();

            return m;
        }
    }
}