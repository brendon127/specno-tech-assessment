﻿using System.Collections.Generic;

namespace Specno.MailExchange.Contracts
{
    public class EmailMessage
    {
        public string Subject { get; set; }
        public string Recipient { get; set; }
        public string Body { get; set; }
        public string Sender { get; set; }
        public string ReplyTo { get; set; }
        public IEnumerable<string> ReplyTos { get; set; } = new List<string>();
        public IEnumerable<string> ToList { get; set; } = new List<string>();
        public IEnumerable<string> CcList { get; set; } = new List<string>();
        public IEnumerable<string> BccList { get; set; } = new List<string>();
    }
}