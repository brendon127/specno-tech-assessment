using System.Threading.Tasks;

namespace Specno.MailExchange.Contracts
{
    public interface IMailExchange
    {
        Task Process(EmailMessage message);
    }
}