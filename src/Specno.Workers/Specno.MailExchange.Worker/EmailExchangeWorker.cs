using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Exceptions;
using Specno.MailExchange.Contracts;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace Specno.MailExchange.Worker
{
    public class EmailExchangeWorker : BackgroundService
    {
        private readonly ILogger<EmailExchangeWorker> _logger;
        private readonly IConfiguration _configuration;
        private readonly IMailExchange _mailExchange;
        private ConnectionFactory _connectionFactory;
        private IConnection _connection;
        private IModel _channel;
        private const string QueueName = "email.exchange";

        public EmailExchangeWorker(ILogger<EmailExchangeWorker> logger, IConfiguration configuration, IMailExchange mailExchange)
        {
            _logger = logger;
            _configuration = configuration;
            _mailExchange = mailExchange;
        }

        public override Task StartAsync(CancellationToken cancellationToken)
        {
            _connectionFactory = new ConnectionFactory
            {
                Uri = new Uri(_configuration["RabbitMQ:DefaultConnection"]),
                DispatchConsumersAsync = true,
            };
            _connection = _connectionFactory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.QueueDeclare("email.exchange",
                durable: true,
                exclusive: false,
                autoDelete: false,
                arguments: null);
            _logger.LogInformation($"Queue [{QueueName}] is waiting for messages.");

            return base.StartAsync(cancellationToken);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.ThrowIfCancellationRequested();

            var consumer = new AsyncEventingBasicConsumer(_channel);
            consumer.Received += consumerOnReceived();
            _channel.BasicConsume(queue: QueueName, autoAck: true, consumer: consumer);
            await Task.CompletedTask;
        }

        private AsyncEventHandler<BasicDeliverEventArgs> consumerOnReceived()
        {
            return async (bc, ea) =>
            {
                var messageString = Encoding.UTF8.GetString(ea.Body.ToArray());
                try
                {
                    var message = JsonSerializer.Deserialize<EmailMessage>(messageString);
                    await _mailExchange.Process(message);
                    if (message != null)
                    {
                        _logger.LogInformation(
                            $"Message with subject: {message.Subject} received");
                    }
                }
                catch (JsonException)
                {
                    _logger.LogError($"JSON Parse Error: '{messageString}'.");
                }
                catch (AlreadyClosedException)
                {
                    _logger.LogInformation("RabbitMQ is closed!");
                }
                catch (Exception e)
                {
                    _logger.LogError(default, e, e.Message);
                }
            };
        }
    }
}