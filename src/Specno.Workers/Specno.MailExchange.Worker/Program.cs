using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Specno.MailExchange.Contracts;

namespace Specno.MailExchange.Worker
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddHostedService<EmailExchangeWorker>();
                    services.AddTransient<IMailExchange, MailExchange>();
                });
    }
}