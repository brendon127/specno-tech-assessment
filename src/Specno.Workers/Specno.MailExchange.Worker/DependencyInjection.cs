using Microsoft.Extensions.DependencyInjection;
using Specno.MailExchange.Contracts;

namespace Specno.MailExchange.Worker
{
    public static class DependencyInjection
    {
        public static void AddEmailExchange(this IServiceCollection services)
        {
            services.AddTransient<IMailExchange, MailExchange>();
        }
    }
}