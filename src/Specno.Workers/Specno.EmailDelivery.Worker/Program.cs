using System.Reflection;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Specno.Domain;
using Specno.MailApplication.Application.Common.Interfaces;
using Specno.MailApplication.Infrastructure;
using Specno.MailDelivery;

namespace Specno.EmailDelivery.Worker
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddHostedService<EmailDeliveryWorker>();
                    services.AddTransient<IIncomingEmailService, IncomingEmailService>();
                    services.AddTransient<ICurrentUserService, CurrentUserService>();
                    services.AddMediatR(typeof(Program).GetTypeInfo().Assembly);
                    services.AddInfrastructure(hostContext.Configuration);
                    services.AddDomain();
                });
    }
}