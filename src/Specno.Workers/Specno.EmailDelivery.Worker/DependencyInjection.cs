using System.Reflection;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Specno.Domain;
using Specno.MailApplication.Application.Common.Interfaces;
using Specno.MailDelivery;

namespace Specno.EmailDelivery.Worker
{
    public static class DependencyInjection
    {
        public static void AddEmailDeliveryWorker(this IServiceCollection services)
        {
            services.AddTransient<IIncomingEmailService, IncomingEmailService>();
        }
    }
}