using Specno.MailApplication.Application.Common.Interfaces;

namespace Specno.EmailDelivery.Worker
{
    public class CurrentUserService : ICurrentUserService
    {
        public string UserId => "[System]";
        public string Email => "[System]";
    }
}