using System;
using System.Collections.Generic;
using System.Linq;
using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NSwag;
using NSwag.AspNetCore;
using NSwag.Generation.Processors.Security;
using Specno.Domain;
using Specno.EmailApi.Filters;
using Specno.EmailApi.Swagger;
using Specno.EmailApi.Utils;
using Specno.EmailDelivery.Worker;
using Specno.MailApplication.Application;
using Specno.MailApplication.Application.Common.Interfaces;
using Specno.MailApplication.Infrastructure;
using Specno.MailApplication.Infrastructure.EF.Contexts;
using Specno.MailExchange.Worker;
using CurrentUserService = Specno.EmailApi.Services.CurrentUserService;

namespace Specno.EmailApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment environment)
        {
            Configuration = configuration;
            Environment = environment;
        }

        public IConfiguration Configuration { get; }

        public IWebHostEnvironment Environment { get; }

        readonly string allowedOrigins = "allowedOrigins";

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddApplication();
            services.AddInfrastructure(Configuration, Environment);
            services.AddDomain();
            
            services.AddSingleton<ICurrentUserService, CurrentUserService>();

            services.AddHostedService<EmailDeliveryWorker>();
            services.AddHostedService<EmailExchangeWorker>();
            services.AddEmailDeliveryWorker();
            services.AddEmailExchange();

            services.AddHttpContextAccessor();

            services.AddDatabaseDeveloperPageExceptionFilter();

            services.AddHealthChecks()
                .AddDbContextCheck<ApplicationDbContext>();

            ValidatorOptions.Global.PropertyNameResolver = CamelCasePropertyNameResolver.ResolvePropertyName;

            services.AddControllers(options =>
                    options.Filters.Add<ApiExceptionFilterAttribute>())
                .AddFluentValidation();

            var originWhitelist = Configuration.GetSection("CORS:Whitelist").Get<List<string>>().ToArray();

            services.AddCors(c =>
            {
                c.AddPolicy(allowedOrigins, options =>
                    {
                        options
                            .WithOrigins(originWhitelist)
                            .AllowAnyMethod()
                            .AllowAnyHeader();
                    }
                );
            });

            services.AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer("Bearer", o =>
                {
                    o.Authority = Configuration.GetValue<string>("Authentication:Domain");
                    o.Audience = Configuration.GetValue<string>("Authentication:Audience");
                    o.RequireHttpsMetadata = false;
                });

            services.AddApiVersioning(options =>
            {
                options.ReportApiVersions = true;
                options.AssumeDefaultVersionWhenUnspecified = true;
                options.DefaultApiVersion = ApiVersion.Default;
                options.ApiVersionReader = new UrlSegmentApiVersionReader();
            }).AddVersionedApiExplorer(options =>
            {
                options.DefaultApiVersion = ApiVersion.Default;
                options.GroupNameFormat = "'v'VVV";
                options.AssumeDefaultVersionWhenUnspecified = true;
                options.SubstituteApiVersionInUrl = true;
            });


            // Customise default API behaviour
            services.Configure<ApiBehaviorOptions>(options => { options.SuppressModelStateInvalidFilter = true; });

            var versions = new[]
            {
                new Version(1, 0),
            };

            foreach (var version in versions)
            {
                services.AddOpenApiDocument(configure =>
                {
                    configure.Title = "Specno Email API";
                    configure.AddSecurity("oauth2", Enumerable.Empty<string>(), new OpenApiSecurityScheme
                    {
                        Type = OpenApiSecuritySchemeType.OAuth2,
                        Flows = new OpenApiOAuthFlows
                        {
                            Implicit = new OpenApiOAuthFlow
                            {
                                AuthorizationUrl =
                                    new string(
                                        $"{Configuration["Authentication:Domain"]}/authorize?audience={Configuration["Authentication:Audience"]}"),
                                Scopes = new Dictionary<string, string>
                                {
                                    {
                                        "openid profile email https://specno-email-api/email", "Get all required info from Auth0"
                                    },
                                }
                            }
                        }
                    });

                    configure.DocumentName = "v" + version.Major;
                    configure.ApiGroupNames = new[] {"v" + version.Major};
                    configure.Version = version.Major + "." + version.Minor;

                    configure.OperationProcessors.Add(new AspNetCoreOperationSecurityScopeProcessor("oauth2"));
                    configure.OperationProcessors.Add(new OrderEndpointsBasedOnConsumer());

                    configure.AllowReferencesWithProperties = true;
                });
            }

            services.AddControllersWithViews();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseMigrationsEndPoint();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHealthChecks("/health");

            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseSwaggerUi3(c =>
            {
                c.OAuth2Client = new OAuth2ClientSettings
                {
                    ClientId = Configuration["Authentication:ClientId"],
                };
            });

            app.UseReDoc(x =>
            {
                x.Path = "/redoc";
                x.DocumentPath = "/api/specification.json";
            });
            app.UseOpenApi();


            app.UseRouting();
            app.UseCors(allowedOrigins);

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints => endpoints.MapControllers());
        }
    }
}