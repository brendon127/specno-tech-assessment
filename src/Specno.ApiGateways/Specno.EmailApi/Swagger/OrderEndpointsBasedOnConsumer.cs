using NSwag.Generation.Processors;
using NSwag.Generation.Processors.Contexts;

namespace Specno.EmailApi.Swagger
{
    public class OrderEndpointsBasedOnConsumer : IOperationProcessor
    {
        public bool Process(OperationProcessorContext context)
        {
            var controllerTypeNamespace = context.ControllerType.Namespace;
            if (controllerTypeNamespace == null) return true;

            var operationTags = context.OperationDescription.Operation.Tags;
            var updated = "";

            foreach (var (consumer, displayName) in ApiConfig.Consumers)
            {
                if (controllerTypeNamespace.Contains(consumer))
                {
                    updated = operationTags[0].Insert(0, $"{displayName}{ApiConfig.Delimiter}");
                }
            }
            operationTags[0] = updated;
            return true;
        }
    }
}