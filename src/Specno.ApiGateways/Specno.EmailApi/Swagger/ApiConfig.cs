using System.Collections.Generic;

namespace Specno.EmailApi.Swagger
{
    public static class ApiConfig
    {
        public static IDictionary<string, string> Consumers = new Dictionary<string, string>
        {
            {nameof(Controllers.App), "App"},
        };

        public static string Delimiter => " - ";
    }
}