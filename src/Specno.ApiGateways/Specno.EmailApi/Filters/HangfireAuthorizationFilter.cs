using Hangfire.Dashboard;

namespace Specno.EmailApi.Filters
{
    public class HangfireAuthorizationFilter : IDashboardAuthorizationFilter
    {
        public bool Authorize(DashboardContext context)
        {
            var httpContext = context.GetHttpContext();

            // Allow all authenticated users to see the Dashboard (potentially dangerous).
            return httpContext.User.Identity != null && httpContext.User.Identity.IsAuthenticated;
        }
    }
}