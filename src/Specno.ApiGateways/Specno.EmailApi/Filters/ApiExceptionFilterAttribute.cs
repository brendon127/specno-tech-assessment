using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Specno.Domain.Exceptions;
using Specno.MailApplication.Application.Common.Exceptions;
using Specno.MailApplication.Application.Common.Models;

namespace Specno.EmailApi.Filters
{
    public class ApiExceptionFilterAttribute : ExceptionFilterAttribute
    {
        private readonly IDictionary<Type, Action<ExceptionContext>> _exceptionHandlers;

        public ApiExceptionFilterAttribute()
        {
            // Register known exception types and handlers.
            _exceptionHandlers = new Dictionary<Type, Action<ExceptionContext>>
            {
                {typeof(ValidationException), HandleValidationException},
                {typeof(IdentityException), HandleIdentityException},
                {typeof(DomainException), HandleForbiddenAccessException},
                {typeof(NotFoundException), HandleNotFoundException},
                {typeof(UnauthorizedAccessException), HandleUnauthorizedAccessException},
                {typeof(ForbiddenAccessException), HandleForbiddenAccessException},
                {typeof(ServiceUnavailableException), HandleServiceUnavailableException},
                {typeof(ApplicationException), HandleApplicationException},
            };
        }

        private void HandleApplicationException(ExceptionContext context)
        {
             var details = new ProblemDetails
            {
                Status = StatusCodes.Status422UnprocessableEntity,
                Title = "UnprocessableEntity",
                Type = "https://httpstatuses.com/422",
                Detail = context.Exception.Message,
            };

            context.Result = new ObjectResult(details)
            {
                StatusCode = StatusCodes.Status422UnprocessableEntity
            };

            context.ExceptionHandled = true;
        }

        public override void OnException(ExceptionContext context)
        {
            HandleException(context);

            base.OnException(context);
        }

        private void HandleException(ExceptionContext context)
        {
            Type type = context.Exception.GetType();
            if (_exceptionHandlers.ContainsKey(type))
            {
                _exceptionHandlers[type].Invoke(context);
                return;
            }

            if (!context.ModelState.IsValid)
            {
                HandleInvalidModelStateException(context);
                return;
            }

            HandleUnknownException(context);
        }

        private void HandleUnknownException(ExceptionContext context)
        {
            var details = new ProblemDetails
            {
                Status = StatusCodes.Status500InternalServerError,
                Title = "An error occurred while processing your request.",
                Type = "https://httpstatuses.com/500"
            };

            context.Result = new ObjectResult(details)
            {
                StatusCode = StatusCodes.Status500InternalServerError
            };

            context.ExceptionHandled = true;
        }

        private void HandleValidationException(ExceptionContext context)
        {
            if (context.Exception is ValidationException exception)
            {
                var details = new ValidationProblemDetails(exception.Errors)
                {
                    Type = "https://httpstatuses.com/400"
                };
                context.Result = new BadRequestObjectResult(details);
            }

            context.ExceptionHandled = true;
        }

        private void HandleIdentityException(ExceptionContext context)
        {
            if (context.Exception is IdentityException exception)
            {
                var details = new ValidationProblemDetails(exception.Errors)
                {
                    Type = "https://httpstatuses.com/400",
                };
                context.Result = new BadRequestObjectResult(details);
            }

            context.ExceptionHandled = true;
        }

        private void HandleInvalidModelStateException(ExceptionContext context)
        {
            var exception = new ValidateModelException(context.ModelState);

            context.Result =
                new BadRequestObjectResult(ServiceResult.Failed(exception.Errors, ServiceError.ValidationFormat));

            context.ExceptionHandled = true;
        }

        private void HandleNotFoundException(ExceptionContext context)
        {
            var detailMessage = context.Exception is NotFoundException exception
                ? exception.Message
                : ServiceError.NotFound.ToString();

            var details2 = new ProblemDetails
            {
                Type = "https://httpstatuses.com/404",
                Title = "The specified resource was not found.",
                Detail = detailMessage
            };
            context.Result = new NotFoundObjectResult(details2);
            context.ExceptionHandled = true;
        }

        private void HandleForbiddenAccessException(ExceptionContext context)
        {
            var details = new ProblemDetails
            {
                Status = StatusCodes.Status403Forbidden,
                Title = "Forbidden",
                Type = "https://httpstatuses.com/403",
                Detail = context.Exception.Message,
            };

            context.Result = new ObjectResult(details)
            {
                StatusCode = StatusCodes.Status403Forbidden
            };

            context.ExceptionHandled = true;
        }

        private void HandleUnauthorizedAccessException(ExceptionContext context)
        {
            var details = new ProblemDetails
            {
                Status = StatusCodes.Status401Unauthorized,
                Title = "Unauthorized",
                Type = "https://httpstatuses.com/401"
            };

            context.Result = new UnauthorizedObjectResult(details);

            context.ExceptionHandled = true;
        }

        private void HandleServiceUnavailableException(ExceptionContext context)
        {
            var details = new ProblemDetails
            {
                Status = StatusCodes.Status503ServiceUnavailable,
                Title = ServiceError.ServiceProvider.ToString(),
                Type = "https://httpstatuses.com/503"
            };
            context.Result = new ObjectResult(details)
            {
                StatusCode = StatusCodes.Status503ServiceUnavailable
            };
            context.ExceptionHandled = true;
        }
    }
}