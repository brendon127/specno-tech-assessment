using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Specno.EmailApi.Controllers.BaseControllers;
using Specno.MailApplication.Application.Onboard.Commands.Onboard;

namespace Specno.EmailApi.Controllers.App.V1
{
    public class OnboardController : AppBaseController
    {
        [HttpPost]
        public async Task<IActionResult> Onboard()
        {
            await Mediator.Send(new OnboardInboxOwnerCommand());
            return NoContent();
        }
    }
}