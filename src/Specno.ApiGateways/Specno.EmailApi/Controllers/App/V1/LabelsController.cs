using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Specno.EmailApi.Controllers.BaseControllers;
using Specno.MailApplication.Application.Common.Models;
using Specno.MailApplication.Application.Labels.Commands.Create;
using Specno.MailApplication.Application.Labels.Commands.Delete;
using Specno.MailApplication.Application.Labels.Queries;

namespace Specno.EmailApi.Controllers.App.V1
{
    public class LabelsController : AppBaseController
    {
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CreateUserDefinedLabelCommand userDefinedLabelCommand)
        {
            await Mediator.Send(userDefinedLabelCommand);
            return NoContent();
        }

        [HttpGet]
        public async Task<ActionResult<ServiceResult<IEnumerable<UserDefinedLabelDto>>>> Get()
        {
            var labels = await Mediator.Send(new GetUserDefinedLabelsQuery());
            return Ok(labels);
        }

        [HttpDelete("{LabelId:Guid}")]
        public async Task<IActionResult> Delete([FromRoute] DeleteUserDefinedLabelCommand deleteUserDefinedLabelCommand)
        {
            await Mediator.Send(deleteUserDefinedLabelCommand);
            return NoContent();
        }
    }
}