using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Specno.Domain.Entities.Label;
using Specno.EmailApi.Controllers.BaseControllers;
using Specno.MailApplication.Application.Common.Models;
using Specno.MailApplication.Application.Message.Commands.AddLabel;
using Specno.MailApplication.Application.Message.Commands.MoveToTrash;
using Specno.MailApplication.Application.Message.Commands.RecoverFromTrash;
using Specno.MailApplication.Application.Message.Commands.RemoveLabel;
using Specno.MailApplication.Application.Message.Commands.Send;
using Specno.MailApplication.Application.Message.Queries;

namespace Specno.EmailApi.Controllers.App.V1
{
    public class MessagesController : AppBaseController
    {
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] SendMessageCommand sendMessageCommand)
        {
            await Mediator.Send(sendMessageCommand);
            return NoContent();
        }

        [HttpPost("{messageId:Guid}/move-to-trash")]
        public async Task<IActionResult> MoveToTrash([FromRoute] MoveMessageToTrashCommand messageToTrashCommand)
        {
            await Mediator.Send(messageToTrashCommand);
            return NoContent();
        }
        
        [HttpPost("{messageId:Guid}/recover-from-trash")]
        public async Task<IActionResult> RecoverFromTrash([FromRoute] RecoverMessageFromTrashCommand recoverMessageFromTrashCommand)
        {
            await Mediator.Send(recoverMessageFromTrashCommand);
            return NoContent();
        }

        [HttpGet]
        public async Task<ActionResult<ServiceResult<IEnumerable<MessageDto>>>> Get(
            [FromQuery] GetMessagesQuery getMessagesQuery)
        {
            var messages = await Mediator.Send(getMessagesQuery);
            return Ok(messages);
        }

        [HttpGet("inbox")]
        public async Task<ActionResult<ServiceResult<IEnumerable<MessageDto>>>> Inbox()
        {
            var query = new GetMessagesQuery()
            {
                LabelId = SystemDefinedLabel.Inbox.Value.ToString()
            };
            var messages = await Mediator.Send(query);
            return Ok(messages);
        }

        [HttpGet("trash")]
        public async Task<ActionResult<ServiceResult<IEnumerable<MessageDto>>>> Trash()
        {
            var query = new GetMessagesQuery()
            {
                LabelId = SystemDefinedLabel.Trash.Value.ToString()
            };
            var messages = await Mediator.Send(query);
            return Ok(messages);
        }

        [HttpGet("sent")]
        public async Task<ActionResult<ServiceResult<IEnumerable<MessageDto>>>> Sent()
        {
            var query = new GetMessagesQuery()
            {
                LabelId = SystemDefinedLabel.Sent.Value.ToString()
            };
            var messages = await Mediator.Send(query);
            return Ok(messages);
        }

        [HttpGet("allMail")]
        public async Task<ActionResult<ServiceResult<IEnumerable<MessageDto>>>> AllMail()
        {
            var query = new GetMessagesQuery()
            {
                LabelId = SystemDefinedLabel.AllMail.Value.ToString()
            };
            var messages = await Mediator.Send(query);
            return Ok(messages);
        }

        [HttpPatch("{messageId:Guid}/label")]
        public async Task<IActionResult> AddLabel([FromRoute] Guid messageId,
            [FromBody] AddLabelToMessageCommand addLabelToMessageCommand)
        {
            addLabelToMessageCommand.MessageId = messageId;
            await Mediator.Send(addLabelToMessageCommand);
            return NoContent();
        }

        [HttpDelete("{messageId:Guid}/label")]
        public async Task<IActionResult> RemoveLabel([FromRoute] Guid messageId,
            [FromBody] RemoveLabelFromMessageCommand removeLabelFromMessageCommand)
        {
            removeLabelFromMessageCommand.MessageId = messageId;
            await Mediator.Send(removeLabelFromMessageCommand);
            return NoContent();
        }
    }
}