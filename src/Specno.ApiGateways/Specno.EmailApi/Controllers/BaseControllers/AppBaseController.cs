using Microsoft.AspNetCore.Mvc;

namespace Specno.EmailApi.Controllers.BaseControllers
{
    [ApiController]
    [Route("v{version:apiVersion}/[controller]")]
    public abstract class AppBaseController : BaseApiController
    {

    }
}