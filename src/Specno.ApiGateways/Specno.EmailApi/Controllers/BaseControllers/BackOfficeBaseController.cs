using Microsoft.AspNetCore.Mvc;

namespace Specno.EmailApi.Controllers.BaseControllers
{
    [ApiController]
    [Route("v{version:apiVersion}/BackOffice/[controller]")]
    public abstract class BackOfficeBaseController : BaseApiController
    {
        
    }
}