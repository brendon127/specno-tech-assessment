﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Specno.EmailApi.Utils;

namespace Specno.EmailApi.Controllers.BaseControllers
{
    [Route("api/")]
    [Authorize]
    [ApiConventionType(typeof(ApiConventions))]
    public abstract class BaseApiController : ControllerBase
    {
        private IMediator _mediator;

        protected IMediator Mediator => _mediator ??= HttpContext.RequestServices.GetService<IMediator>();
    }
}