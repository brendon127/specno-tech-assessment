# Specno Email API

The ERD.png and Architecture.png diagrams can be found in the root of the project.
The project is implemented using Domain-Driven Design, Clean Architecture and CQRS.

Unfortunately I could not get the api hosted on Azure. RabbitMQ did not play nice in the App Service.
There were some outbound traffic issues. However, I added the database and rabbitmq connection strings to the appSettings for ease of installation.
I will never add these secrets to the appsettings in a production environment.

## Prerequisites
1. [Docker](https://www.docker.com/)
2. [.NET Core](https://docs.microsoft.com/en-us/dotnet/core/install/macos)
3. C# IDE
    * [Visual Studio](https://docs.microsoft.com/en-us/visualstudio/install/install-visual-studio?view=vs-2019)
    * [Visual Studio Code](https://code.visualstudio.com/Download)
    * [Rider](https://www.jetbrains.com/help/rider/Installation_guide.html)
4. [SQL Server (via docker)](https://hub.docker.com/_/microsoft-mssql-server). See Database section below.
4. [Rabbit Mq(via docker)](https://hub.docker.com/_/rabbitmq). See Rabbit section below.

## Setup
1. Clone the repo.
2. Open it in the IDE of your choice (Visual Studio, Visual Studio Code or Rider).
3. Restore the Nuget packages with the following command: `dotnet restore`

## Run
### Database
To run the database, use the following command:
```bash
docker run -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=P@ssword1' -p 1433:1433 --name msql -d mcr.microsoft.com/mssql/server:2019-latest
```

### Rabbit
To run the broker, use the following command:
```bash
docker run -d --hostname my-rabbit --name rabbit -p 15672:15672 -p 5672:5672 rabbitmq:3-management
```

### Project
To build and run the project, use the following commands (replace {Project} with the name of the project):
```bash
cd src/Specno.ApiGateways/Specno.EmailApi
dotnet restore
dotnet build
dotnet run
```

## Swagger Documentation
To access the Swagger docs, use the following URL pattern `{domain}/swagger`.
This URL on development is: `https://localhost:5021/swagger`.  

###  Logout Url
Visit the following url to log out of auth0 account.
https://specno-tech-assesment.eu.auth0.com/v2/logout?%20client_id=5A9vvCTyzNemmvNPZjqiGMh6KAhy9wQ7&%20returnTo=LOGOUT_URL

## Authentication
Authentication is implemented using OpenId Connect with Auth0.

I've created 3 users that you can sign in as.

* brenvb@pm.me
* brendonvbiljoen@protonmail.com
* brendon@coolskins.co.za

They all have the same password - P@ssword1

You are welcome to create sign up your own email address or continue with your google account.
**Note**: Once you have signed in with your new account you will have to call the **Onboard** endpoint before you'll be able to call any of the other endpoints. 
The endpoint would normally automatically be called on the redirect back to the frontend but since we only have swagger we have to manually do it.
